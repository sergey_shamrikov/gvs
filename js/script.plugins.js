
var PluginsInit = {

	swiperProductMain() {

		if ($('.product_slider_main').length) {

			const swiperThumbs = new Swiper('.product_slider_thumbs', {
				// Optional parameters
				slidesPerView: 6,
				spaceBetween: 10,
				freeMode: true,
				watchSlidesProgress: true,
			});
			const swiperProduct1 = new Swiper('.product_slider_main', {
				// Optional parameters
				slidesPerView: 1,
				spaceBetween: 30,
				navigation: {
					nextEl: ".swiper-button-next",
					prevEl: ".swiper-button-prev",
				},
				// If we need pagination
				pagination: {
					el: '.swiper-pagination',
					clickable: true
				},
				thumbs: {
					swiper: swiperThumbs,
				}
			});

		}

	},

	swiperSection() {

		if ($('.section_slider').length) {
			const swiperSection = new Swiper('.section_slider', {
				// Optional parameters
				slidesPerView: 1,
				spaceBetween: 30,

				// Navigation arrows
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev',
				},
				breakpoints: {
					576: {
						slidesPerView: 2
					},
					992: {
						slidesPerView: 3
					},
				}
			});

		}

	},

	swiperProduct() {

		if ($('.product_slider').length) {
			const swiperProduct = new Swiper('.product_slider', {
				// Optional parameters
				slidesPerView: 1,
				spaceBetween: 30,

				// If we need pagination
				pagination: {
					el: '.product_slider_pagination',
					clickable: true
				}
			});

		}

	},

	fancybox() {

		if ($('.fancybox').length) {


			if ($(window).width() < 992) {

				jQuery('.fancybox').fancybox({
					padding: 0,
					margin: 0,
					helpers: {
						overlay: {
							locked: false // отключаем блокировку overlay
						}
					},

					beforeShow: function () {

						$("body").addClass('body_fix');

					},

					afterClose: function () {

						$("body").removeClass('body_fix');

					}

				});

			} else {

				jQuery('.fancybox').fancybox({
					padding: 0,

					helpers: {
						overlay: {
							locked: false // отключаем блокировку overlay
						}
					}
				});

			}


		}

	},

}

$(document).ready(function () {
	PluginsInit.swiperSection();
	PluginsInit.swiperProduct();
	PluginsInit.swiperProductMain();
	// PluginsInit.fancybox();
});

$(window).on('load', function () {
	// PluginsInit.swiperSoloMain();
});





