
; (function ($) {

	"use strict";

	var Core = {

		DOMReady: function () {

			var self = this;

			self.navResp();
			self.backToTopBtn();
			self.accordion();
			self.filter.init();
			self.tabs.init();
			self.events();
			self.miniCart.init();

		},

		windowLoad: function () {

			var self = this;

			self.preloader();
			self.dropdownMenu();
			self.fixHeder.init();
			self.scrollWidth();

		},

		resize: function () {

			var self = this;
			self.scrollWidth();
		},

		scroll: function () {

			var self = this;
		},

		scrollWidth: function () {
			var self = this;
			// создадим элемент с прокруткой
			let div = document.createElement('div');

			div.style.overflowY = 'scroll';
			div.style.width = '50px';
			div.style.height = '50px';

			// мы должны вставить элемент в документ, иначе размеры будут равны 0
			document.body.append(div);
			self.scrollW = div.offsetWidth - div.clientWidth;

			div.remove();
		},

		// Ivents
		events: function () {

			$(window).on('resize', function () {
				if ($(window).width() > 991) {
					$('.filter-panel-wrapper').removeClass('is-open');
					$('body').removeClass('no-scroll');
				}
			})

			$('.filter-panel-btn').on('click', function () {
				$(this).closest('.filter-panel-wrapper').addClass('is-open');
				$('body').addClass('no-scroll');
			})

			$('.filter-panel-close').on('click', function () {
				$(this).closest('.filter-panel-wrapper').removeClass('is-open');
				$('body').removeClass('no-scroll');
			})

			$('.js-toggle-shipping-selection').on('click', function () {
				$('.shipping-preference').slideToggle(200);
			})
		},

		dropdownMenu: function () {

			var self = this;

			$(".dropdown").on('click', function () {

				$(this).find(".dropdown-menu").toggleClass("show");


			});

			$(document).click(function (event) {
				if ($(event.target).closest(".dropdown").length) return;
				//$("p").hide("slow");
				$('.dropdown-menu').removeClass('show');
				event.stopPropagation();
			});

		},

		/**
		**  fixHederResp
		**/
		fixHeder: {

			init: function () {
				var self = this;
				self.d = $(document);
				self.w = $(window);
				self.header = $('#header');
				self.navWrap = $('.nav-main-wrap');
				self.nav = $('.nav-main');

				self.headerHeight();
				self.checkPositionHeader();

				self.w.on('resize', function () {
					self.headerHeight();
				})

				self.w.on('scroll', function () {
					self.checkPositionHeader();
				})

			},

			headerHeight: function () {
				var self = this;
				self.navWrap.innerHeight(self.nav.height());
			},

			checkPositionHeader: function () {
				var self = this;
				if (self.w.width() > 1200) {
					if (self.w.scrollTop() >= self.navWrap.offset().top) {

						self.header.addClass('fix');

					}
					else {
						self.header.removeClass('fix');
					}
				}
				else {
					self.header.removeClass('fix');
				}
			}
		},

		/**
		**  Nav Resp
		**/
		navResp: function () {

			$('.navigation-flyout').prev('.nav-link').addClass('nav-link-arrow');

			$(".nav-main-toggle").on('click', function () {

				$('.main-navigation').toggleClass("is-open");
				$('.modal-overflow').toggleClass("modal-overflow-open");

			});

			$(".menu-button").on('click', function () {

				$('.main-navigation').toggleClass("is-open");
				$('.modal-overflow').toggleClass("modal-overflow-open");

			});

			$(".js-offcanvas-close").on('click', function () {

				$('.main-navigation').removeClass("is-open");
				$('.modal-overflow').removeClass("modal-overflow-open");
				$('.main-navigation').removeClass("open-lv-2");
				$('.navigation-flyout').removeClass("is-open");
				$('.nav-drop').removeClass('hide');

			});

			$(".modal-overflow").on('click', function () {

				$('.main-navigation').removeClass("is-open");
				$('.modal-overflow').removeClass("modal-overflow-open");
				$('.main-navigation').removeClass("open-lv-2");
				$('.navigation-flyout').removeClass("is-open");
				$('.nav-drop').removeClass('hide');

			});

			$(".nav-drop a").on('click', function () {
				if ($(window).width() < 992) {

					event.preventDefault();
					$('.navigation-flyout').addClass("is-open");
					$(this).parent('.nav-drop').siblings('.nav-drop').addClass('hide');
					$('.main-navigation').addClass("open-lv-2");
				}

			});

			$(".js-navigation-offcanvas-link").on('click', function () {

				$('.navigation-flyout').removeClass("is-open");
				$('.nav-drop').removeClass('hide');
				$('.main-navigation').removeClass("open-lv-2");

			});

			$(".js-search-toggle-btn").on('click', function () {

				$('.js-search-toggle-btn').toggleClass("collapsed");
				$('.header-search').toggleClass("collapsed");

			});



		},

		/**
		**	Preloader
		**/
		preloader: function () {

			var self = this;

			self.preloader = $('#page-preloader');
			self.spinner = self.preloader.find('.preloader');

			self.spinner.fadeOut();
			self.preloader.delay(350).fadeOut('slow');
		},

		/**
		**  Back to top
		**/
		backToTopBtn: function (config) {

			config = $.extend({
				offset: 350,
				transitionIn: 'show',
				transitionOut: 'hidde'
			}, config);

			var btn = $('.back_to_top'),
				$wd = $(window),
				$html = $('html'),
				$body = $('body');


			$wd.on('scroll.back_to_top', function () {

				if ($wd.scrollTop() > config.offset) {

					btn.removeClass('hide ' + config.transitionOut).addClass(config.transitionIn);

				}
				else {

					btn.removeClass(config.transitionIn).addClass(config.transitionOut);

				}

			});

			btn.on('click', function () {

				$html.add($body).animate({

					scrollTop: 0

				});

			});

		},

		/**
		**  Accordion
		**/
		accordion: function () {

			$('.accordion_title').on('click', function () {
				$(this)
					.toggleClass('active')
					.next('.accordion_content')
					.slideToggle(200);
			})
		},

		/**
		**  Tabs
		**/
		tabs: {

			init: function () {

				var self = this;
				self.w = $(window);
				self.tabs = $('.tabs_wrap');
				self.tabsMob = self.w.width() < 576 ? true : false;

				self.checkMob();
				self.initTabs();
				self.ivents();

				self.w.on('resize', function () {
					self.checkMob();
				})
			},

			checkMob: function () {

				var self = this;

				if ((!self.tabsMob) && self.w.width() < 576) {
					self.initTabs();
					self.tabsMob = true;
				}
				else if (self.tabsMob && self.w.width() > 575) {
					self.initTabs();
					self.tabsMob = false;
				}
			},

			ivents: function () {

				var self = this;
				$('.tabs_nav_item').on('click', function () {
					if ($(this).hasClass('active')) return;
					self.changeTab($(this));
				});

				$('.js-tab-close').on('click', function () {
					$(this).closest('.tab_item').removeClass('active');
					$(this).closest('.tabs_wrap').find('.tabs_nav_item').removeClass('active');
				});
			},

			initTabs: function () {

				var self = this;
				if (self.w.width() < 576) {
					$('.tab_item').removeClass('active');
					$('.tabs_nav_item').removeClass('active');
				}
				else {
					self.tabs.each(function (index, el) {
						var active = $(el).find('.tabs_nav_item.active').length ? $(el).find('.tabs_nav_item.active') : $(el).find('.tabs_nav_item').eq(0);
						self.changeTab(active);
					});
				}
			},
			changeTab: function (el) {

				var self = this;
				var activIndex = el.index();
				var tabActive = el.closest('.tabs_wrap').find('.tab_item').eq(activIndex);
				el.addClass('active').siblings().removeClass('active');
				tabActive.addClass('active').siblings().removeClass('active');
			}
		},

		/**
		**  Filter
		**/
		filter: {

			init: function () {

				var self = this;
				self.label = $('.filter-panel-wrapper').find('.checkbox_square_label');
				self.label.on('click', function () {
					var $this = $(this);
					setTimeout(function () {
						self.calculation($this);
					}, 200)
				});
			},

			calculation: function (el) {

				var titleCount = el.parents('.accordion_content').prev('.accordion_title').find('.filter-count'),
					countCheckboxs = el.closest('.accordion_content').find(".checkbox_square_input:checked").length;
				if (countCheckboxs > 0) {
					titleCount.html("(" + countCheckboxs + ")");
				}
				else {
					titleCount.html("");
				}
			}
		},

		/**
		**  Mini Cart
		**/
		miniCart: {

			init: function () {

				var self = this;
				self.b = $('body');
				self.cart = $('#miniCart');

				self.events();
			},

			events: function () {

				var self = this;

				$('.header-cart-btn, .js-open-cart-btn').on('click', function () {
					self.openCart();
				})

				$('.js-mini_cart-close, .mini_cart_overlay').on('click', function () {
					self.closeCart();
				})
			},
			openCart: function () {
				var self = this;
				self.cart.addClass('is-open');
				self.b.addClass('no-scroll').css({ "padding-right": Core.scrollW });
			},
			closeCart: function () {

				var self = this;
				self.cart.removeClass('is-open');
				self.b.removeClass('no-scroll').css({ "padding-right": 0 });;
			}
		}
	}


	$(document).ready(function () {

		Core.DOMReady();

	});

	$(window).on('load', function () {

		Core.windowLoad();

	});

	$(window).on("scroll", function () {

		Core.scroll();

	});

	$(window).on("resize", function () {

		Core.resize();

	});


})(jQuery);